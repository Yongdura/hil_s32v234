SRC = ./src
SRC_A53 = $(SRC)/a53
SRC_APU = $(SRC)/apu
SRC_ISP = $(SRC)/isp
INC = ./include
APP_NAME = htm

CC = $(CROSS_COMPILE)gcc
CFLAGS = -I$(INC)/. -lpthread
DEPS = $(INC)/arm64_regs.h \
       $(INC)/S32V234.h \
       $(INC)/had-testing-mode.h \
       $(INC)/typedefs.h 
OBJ = $(SRC)/main.o \
      $(SRC_A53)/htm-a53-cores-tasks.o \
      $(SRC_APU)/htm-apu-tasks.o \
      $(SRC_ISP)/htm-isp-tasks.o  

%.o: %.c $(DEPS) 
	$(CC) -c -o $@ $< $(CFLAGS)

$(APP_NAME): $(OBJ) 
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: 
	clean

clean: 
	rm -f $(SRC)/*.o \
	      $(SRC_A53)/*.o \
	      $(SRC_APU)/*.o \
	      $(SRC_ISP)/*.o \
          $(APP_NAME)
