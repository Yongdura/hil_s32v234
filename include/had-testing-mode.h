/*****************************************************************************
						   PVCS REVISION INFO
NOTE: Use keyword expansion here.  Replace the ‘*’ in the five lines following 
this sentence with ‘$’.  You can also remove this note from the template.
These are required, others can be added as desired.

*Revision*
*Author*
*Date*
*Modtime*
*Archive*
         
*******************************************************************************
									FILE INFO
Purpose:
    <Purpose>

Design Document:
	<Title> 

Notes:


*******************************************************************************
						  PROPRIETARY NOTICE

   This copyrighted document is the property of DURA Automotive and is
   disclosed in confidence.  It may not be copied, disclosed to others,
   or used for manufacturing, without the prior written consent of
   DURA Automotive.

					  Copyright 2017 DURA Automotive
******************************************************************************/

#ifndef HAD_TESTING_MODE_H_
#define HAD_TESTING_MODE_H_


#ifndef HTM_GLOBAL_VAR
#define HTM_GV_EXTERN extern
#else
#define HTM_GV_EXTERN  
#endif

#ifndef HTM_STATIC_VAR
#define HTM_SV_STATIC static
#else
#define HTM_SV_STATIC
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

/******************************************************************************
* 
*   INCLUDE FILES
*
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <sched.h>
#include <string.h>

#include "S32V234.h"                                                                                                                       /* include peripheral declarations */
#include "typedefs.h"


/******************************************************************************
*   
*   DEFINITIONS
*
******************************************************************************/
#define HTM_APP_STAT_INIT                            ((uint32_t)0)                                                                         /* The application in initial stage */
#define HTM_APP_STAT_RUN                             ((uint32_t)1)                                                                         /* The application begin to run after initialization done */
#define HTM_APP_STAT_TRYING                          ((uint32_t)2)
#define HTM_APP_STAT_EXIT                            ((uint32_t)255)                                                                       /* The application is ready to exit */

#define HTM_ERROR_SAVE_PRINT_EXIT(en, msg)           do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)                         /* Save error number, print error message then exit with failure */
#define HTM_ERROR_PRINT_EXIT(msg)                    do { perror(msg); exit(EXIT_FAILURE); } while (0)                                     /* Print error message then exit with failure */

#define HTM_AFFIN_SUCCESS_MSG                        "Successfully set thread %lu to affinity to CPU %d\n"                                 /* Success message to indicate a task has been move to a A53 core for execution */
#define HTM_AFFIN_FAILURE_MSG                        "Failed to set thread %lu to affinity to CPU %d\n"                                    /* Failure message to indicate failed to assign a A53 core for a task execution */

#define HTM_S32V_A53_CORES_NUM                       (uint32_t)(4)                                                                         /* How many A53 CPU cores in S32V234 platform */
#define HTM_S32V_A53_TASKS_NUM                       (uint32_t)(4)                                                                         /* How many tasks allowed to be executed in A53 cores */
#define HTM_S32V_APU_TASKS_NUM                       (uint32_t)(4)                                                                         /* How many APEX tasks allowed to be executed in APU */
#define HTM_S32V_APU_TASKS_NUM_PER_CPU               (uint32_t)(1)                                                                         /* How many APEX tasks assigned to a CPU core */
#define HTM_S32V_ISP_TASKS_NUM                       (uint32_t)(4)
#define HTM_S32V_ISP_TASKS_NUM_PER_CPU               (uint32_t)(1)

#define HTM_APP_SEL_TRY_LIMIT                        (uint32_t)(5)                                                                         /* Maximum times to try to select a valid test */
#define HTM_APP_SEL_NUM_MIN                          (uint32_t)(0)                                                                         /* Minimum choice in the application */
#define HTM_APP_SEL_NUM_MAX                          HTM_APP_SEL_MAX_VALID_ID                                                              /* Maxinum choice in the application */

#define HTM_APP_SEL_A53_TEST_ID                      (uint32_t)(0)
#define HTM_APP_SEL_APU_TEST_ID                      (uint32_t)(1)
#define HTM_APP_SEL_ISP_TEST_ID                      (uint32_t)(2)
#define HTM_APP_SEL_GPU_TEST_ID                      (uint32_t)(3)
#define HTM_APP_SEL_EXIT_ID                          (uint32_t)(4)
#define HTM_APP_SEL_SETTING_ID                       (uint32_t)(5)
#define HTM_APP_SEL_MAX_VALID_ID                     HTM_APP_SEL_SETTING_ID

#define HTM_APP_SEL_A53_TEST_EN                      1u                                                                         /* Enable/Disable function of A53 test */
#define HTM_APP_SEL_APU_TEST_EN                      1u                                                                         /* Enable/Disable function of APU test */
#define HTM_APP_SEL_ISP_TEST_EN                      1u                                                                         /* Enable/Disable function of ISP test */
#define HTM_APP_SEL_GPU_TEST_EN                      1u                                                                         /* Enable/Disable function of GPU test */

#define HTM_APP_PROC_RT_PERIOD                       ((uint32_t)30)                                                                         /* How long will the process run in seconds */

#define HTM_STR_MAX_NUM                              ((uint32_t)255)
#define HTM_CPU_ID_MAX_LEN                           ((uint32_t)5)
#define HTM_CPU_LINE_DELIMITER                       " "

typedef void * (*TASK_ENTRY_PTR_T)(void *);                                                                                                /* Type of entry pointer of a A53/APU task */

typedef struct TASK_INFO_TAG{
	pthread_t          Task_ID;            						                                                                           /* Thread ID returned by pthread_create() */
	uint32_t           A53_Core_ID;               						                                                                   /* A53 core id is assigned to run a task */
}TASK_INFO_T;


/******************************************************************************
*   
*   VARIABLE DECLARATIONS
*
******************************************************************************/
HTM_GV_EXTERN uint32_t             HTM_App_Status;                                               
HTM_GV_EXTERN uint32_t             HTM_App_Sel_Retry;                                                                                    /* How many times can you try before selecting a valid test */

HTM_GV_EXTERN struct timespec      HTM_Proc_Tm_Start;
HTM_GV_EXTERN struct timespec      HTM_Proc_Tm_End;
HTM_GV_EXTERN uint32_t             HTM_Proc_Exec_Period;

HTM_GV_EXTERN uint32_t             HTM_CPU_Usage_Prev_Totoal;                                                                           /* The total amount of time the four CPU cores have spent performing different kinds of work */
HTM_GV_EXTERN uint32_t             HTM_CPU_Usage_Prev_Idle;                                                                             /* The amount of time CPU cores in idle */
HTM_GV_EXTERN uint32_t             HTM_CPU_Core_Usage_Prev_Totoal[HTM_S32V_A53_CORES_NUM];                                              /* The total amount of time the four CPU core i have spent performing different kinds of work */
HTM_GV_EXTERN uint32_t             HTM_CPU_Core_Usage_Prev_Idle[HTM_S32V_A53_CORES_NUM];                                                /* The amount of time CPU core i in idle */

/******************************************************************************
*   
*   FUNCTION DECLARATIONS
*
******************************************************************************/

/* ****************************************************************
 * A53 Cores Stress Test
 * ***************************************************************/
extern int32_t            A53_Test                                   (void                                                             );
extern uint32_t           A53_Create_Tasks                           (uint32_t         core_id,         TASK_ENTRY_PTR_T        t_entry);
extern void *             A53_Alloc_Core                             (TASK_INFO_T *    t_info                                          );
extern void *             Task_A53_Multicore                         (void *           arg                                             );


/* ****************************************************************
 * APU Stress Test
 * ***************************************************************/
extern int32_t            Apex_Test                                  (void                                                             );
extern void *             Task_Apex_Add                              (void *           arg                                             );
extern void *             Task_Apex_Downsample_Upsample_cv           (void *           arg                                             );
extern void *             Task_Apex_Face_Detection_cv                (void *           arg                                             );
extern void *             Task_Apex_Fast9_cv                         (void *           arg                                             );
extern void *             Task_Apex_Gauss5x5_cv                      (void *           arg                                             );
extern void *             Task_Apex_Histogram_cv                     (void *           arg                                             );
extern void *             Task_Apex_Indirect_Input_cv                (void *           arg                                             );
extern void *             Task_Apex_Integral_Image_cv                (void *           arg                                             );
extern void *             Task_Apex_Irq                              (void *           arg                                             );
extern void *             Task_Apex_Isp_Fast9                        (void *           arg                                             );
extern void *             Task_Apex_Orb_cv                           (void *           arg                                             );
extern void *             Task_Apex_Roi_cv                           (void *           arg                                             );

/* ****************************************************************
 * ISP Stress Test
 * ***************************************************************/
extern int32_t            Isp_Test                                   (void                                                             );
extern void *             Task_Isp_Csi_Dcu                           (void *           arg                                             );
extern void *             Task_Isp_Generic                           (void *           arg                                             );
extern void *             Task_Isp_Jpeg                              (void *           arg                                             );
extern void *             Task_Isp_Ov10640_Quad                      (void *           arg                                             );
extern void *             Task_Isp_Rgb_Yuv_Gs8                       (void *           arg                                             );
extern void *             Task_Isp_Sony_Dualexp                      (void *           arg                                             );
extern void *             Task_Isp_Viu_Dcu                           (void *           arg                                             );
extern void *             Task_Isp_Yuv_Grey_Pyramid                  (void *           arg                                             );


/* ****************************************************************
 * Helper Functions
 * ***************************************************************/
extern void               HTM_Gui_Init                               (void                                                             );
extern float              HTM_Proc_Tm_Diff                           (struct timespec * tstart,        struct timespec *           tend);
extern uint32_t           HTM_Get_A53_CPU_Usage                      (uint32_t          core_id                                        );
extern void               HTM_Proc_Set_Exec_Period                   (void                                                             );
extern void               HTM_Apex_List_Exec_Proc                    (void                                                             );
extern void               HTM_Isp_List_Exec_Proc                     (void                                                             );
#endif /* HAD_TESTING_MODE_H_ */
