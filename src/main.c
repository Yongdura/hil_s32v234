/*****************************************************************************
						   PVCS REVISION INFO
NOTE: Use keyword expansion here.  Replace the ‘*’ in the five lines following
 this sentence with ‘$’.  You can also remove this note from the template. 
 These are required, others can be added as desired.

*Revision*
*Author*
*Date*
*Modtime*
*Archive*

*******************************************************************************
									FILE INFO
Purpose:
    <Purpose>

Function List:
    <Function List>
                                                         
Design Document:
	<Title> 

Notes:
                                                         

*******************************************************************************
						  PROPRIETARY NOTICE

   This copyrighted document is the property of DURA Automotive and is
   disclosed in confidence.  It may not be copied, disclosed to others,
   or used for manufacturing, without the prior written consent of
   DURA Automotive.

					  Copyright 2017 DURA Automotive
******************************************************************************/

#ifndef HTM_GLOBAL_VAR
#define HTM_GLOBAL_VAR
#endif

/******************************************************************************
*   
*   INCLUDE FILES
*
******************************************************************************/
#include "had-testing-mode.h"

/******************************************************************************
*   
*   CODE
*
******************************************************************************/
int main(void)
{
	/* Initialize GUI for MODE selections */
	/* Select different testing mode to run */
	uint32_t usel = 3;
	char emsg[128];
	int quit_proc = 0;
	int i;

	/* Set application initial status */
	HTM_App_Status = HTM_APP_STAT_INIT;
	/* Initialize limit times to select a valid test */
	HTM_App_Sel_Retry = (uint32_t)0;

	/* Initialize variables about CPU usage */
	HTM_CPU_Usage_Prev_Totoal = (uint32_t)0;
	HTM_CPU_Usage_Prev_Idle = (uint32_t)0;
	for(i = 0; i < HTM_S32V_A53_CORES_NUM; i++)
	{
		HTM_CPU_Core_Usage_Prev_Totoal[i] = (uint32_t)0;
		HTM_CPU_Core_Usage_Prev_Idle[i] = (uint32_t)0;
	}

	/* Set process execution period */
	HTM_Proc_Exec_Period = HTM_APP_PROC_RT_PERIOD;
	
	while(1)
	{
		switch (HTM_App_Status)
		{
			case HTM_APP_STAT_INIT:
			{
				/* Setup GUI */
				HTM_Gui_Init();
				/* Waiting for input of choice */
				scanf("%d", &usel);
				/* Verify if the choice is valid */
				if ((usel >= HTM_APP_SEL_NUM_MIN) && (usel <= HTM_APP_SEL_NUM_MAX))
				{
					/* Reset quit flag for process */
					quit_proc = 0;
					/* Reset try times */
					HTM_App_Sel_Retry = (uint32_t)0;
					/* Valid choice */
					HTM_App_Status = HTM_APP_STAT_RUN;
				}
				else
				{
					/* Invalid choice */
					HTM_App_Status = HTM_APP_STAT_TRYING;
				}
				break;
			}
			case HTM_APP_STAT_TRYING:
			{
				/* Increment until reach the limit */
				HTM_App_Sel_Retry++;
				/* Set application status to TRYING or EXIT */
				if (HTM_App_Sel_Retry < HTM_APP_SEL_TRY_LIMIT)
				{
					HTM_App_Status = HTM_APP_STAT_INIT;
					printf("Select a valid test [Retry left %d times]\n", (HTM_APP_SEL_TRY_LIMIT - HTM_App_Sel_Retry));
				}
				else
				{
					HTM_App_Status = HTM_APP_STAT_EXIT;
					memset((char *)&emsg[0], 0x0, sizeof(emsg));
					sprintf(emsg, "Retry left %d times. EXIT~~~\n", (HTM_APP_SEL_TRY_LIMIT - HTM_App_Sel_Retry));
				}
				break;
			}
			case HTM_APP_STAT_RUN:
			{
				switch (usel)
				{
					case HTM_APP_SEL_A53_TEST_ID: /* case 1. A-53 cores stress test */
					{
						clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_Start);
						/* Keep running the test task */
						while(quit_proc != 1)
						{
							A53_Test();
							/*usleep(10);*/
							/* Check time elapsed */
							clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_End);
							if (HTM_Proc_Exec_Period < HTM_Proc_Tm_Diff(&HTM_Proc_Tm_Start, &HTM_Proc_Tm_End))
							{
								quit_proc = 1;
							}
						}
						HTM_App_Status = HTM_APP_STAT_INIT;
						break;
					}
					case HTM_APP_SEL_APU_TEST_ID: /* case 2. APEX stress test */
					{
						HTM_Apex_List_Exec_Proc();
						printf("Press enter to continue...\n");
						fgetc(stdin);
						fgetc(stdin);

						clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_Start);
						/* Keep running the test task */
						while(quit_proc != 1)
						{
							Apex_Test();
							/*usleep(10);*/
							/* Check time elapsed */
							clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_End);
							if (HTM_Proc_Exec_Period < HTM_Proc_Tm_Diff(&HTM_Proc_Tm_Start, &HTM_Proc_Tm_End))
							{
								quit_proc = 1;
							}
						}
						HTM_App_Status = HTM_APP_STAT_INIT;
						break;
					}
					case HTM_APP_SEL_ISP_TEST_ID:
					{
						HTM_Isp_List_Exec_Proc();
						printf("Press enter to continue...\n");
						fgetc(stdin);
						fgetc(stdin);

						clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_Start);
						/* Keep running the test task */
						while(quit_proc != 1)
						{
							Isp_Test();
							/*usleep(10);*/
							/* Check time elapsed */
							clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_End);
							if (HTM_Proc_Exec_Period < HTM_Proc_Tm_Diff(&HTM_Proc_Tm_Start, &HTM_Proc_Tm_End))
							{
								quit_proc = 1;
							}
						}
						HTM_App_Status = HTM_APP_STAT_INIT;
						break;
					}
					case HTM_APP_SEL_GPU_TEST_ID:
					{
						clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_Start);
						/* Keep running the test task */
						while(quit_proc != 1)
						{
							/*Gpu_Test();*/
							/*usleep(10);*/
							/* Check time elapsed */
							clock_gettime(CLOCK_MONOTONIC, &HTM_Proc_Tm_End);
							if (HTM_Proc_Exec_Period < HTM_Proc_Tm_Diff(&HTM_Proc_Tm_Start, &HTM_Proc_Tm_End))
							{
								quit_proc = 1;
							}
						}
						HTM_App_Status = HTM_APP_STAT_INIT;
						break;
					}
					case HTM_APP_SEL_EXIT_ID:
					{
						/* Normal exit */
						HTM_App_Status = HTM_APP_STAT_EXIT;
						memset((char *)&emsg[0], 0x0, sizeof(emsg));
						sprintf(emsg, "No more tasks executed. EXIT~~~\n");
						break;
					}
					case HTM_APP_SEL_SETTING_ID:
					{
						HTM_Proc_Set_Exec_Period();
						HTM_App_Status = HTM_APP_STAT_INIT;
						break;
					}
					default: /* Invalid cases */
					{
						
						break;
					}
				}
				break;
			}
			case HTM_APP_STAT_EXIT:
			{
				HTM_ERROR_PRINT_EXIT(emsg);
				break;
			}
			default:
			{
				break;
			}
		}
	}

    /* to avoid the warning message for GHS and IAR: statement is unreachable*/
#if defined (__ghs__)
#pragma ghs nowarning 111
#endif
#if defined (__ICCARM__)
#pragma diag_suppress=Pe111
#endif	
	return 0;
}

/******************************************************************************
* FUNCTION NAME: HTM_Gui_Init
*
* PURPOSE FOR FUNCTION: 1. Initialize the user interface
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void HTM_Gui_Init(void)
{
	uint32_t entry_id = (uint32_t)0;

	fputs("--------- HAD Testing Mode ---------\n", stdout        );
#if HTM_APP_SEL_A53_TEST_EN > 0u
	printf("%d. A53 Cores Test\n"                 , entry_id++    );
#endif
#if HTM_APP_SEL_APU_TEST_EN > 0u
	printf("%d. APU Cores Test\n"                 , entry_id++    );
#endif
#if HTM_APP_SEL_A53_TEST_EN > 0u
	printf("%d. ISP Cores Test\n"                 , entry_id++    );
#endif
#if HTM_APP_SEL_A53_TEST_EN > 0u
	printf("%d. GPU Cores Test\n"                 , entry_id++    );
#endif
	fputs("---------------------------------\n"   , stdout        );
	printf("%d. Exit\n"                           , entry_id++    );
	printf("%d. Setting\n"                        , entry_id++    );
	fputs("Enter your choice? [1/2/3...] "        , stdout        );
}

/******************************************************************************
* FUNCTION NAME: HTM_Proc_Set_Exec_Period
*
* PURPOSE FOR FUNCTION: 1. Setting process execution period.
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void HTM_Proc_Set_Exec_Period(void)
{
	uint32_t new_period = (uint32_t)0;

	printf("The process execution period %d\n", HTM_Proc_Exec_Period);
	fputs("Input the new setting: ", stdout);
	scanf("%d", &new_period);
	if (new_period > (uint32_t)0)
	{
		HTM_Proc_Exec_Period = new_period;
	}
}

/******************************************************************************
* FUNCTION NAME: HTM_Proc_Tm_Diff
*
* PURPOSE FOR FUNCTION: 1. Helper function to calculate difference of stat time
*                          and end time
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
float HTM_Proc_Tm_Diff(struct timespec * tstart, struct timespec * tend)
{
	float tdiff;

	tdiff = ((double)tend->tv_sec + 1.0e-9 * tend->tv_nsec) - ((double)tstart->tv_sec + 1.0e-9 * tstart->tv_nsec);

	return tdiff;
}

/******************************************************************************
* FUNCTION NAME: HTM_Proc_Tm_Diff
*
* PURPOSE FOR FUNCTION: 1. Helper function to calculate difference of stat time
*                          and end time
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
uint32_t HTM_Get_A53_CPU_Usage(uint32_t core_id)
{
	uint32_t i;
	uint32_t cpu_kw_cnt = (uint32_t)0;
	uint32_t cpu_tm_val = (uint32_t)0;
	uint32_t cpu_total = (uint32_t)0;
	uint32_t cpu_idle = (uint32_t)0;
	uint32_t cpu_idle_diff = (uint32_t)0;
	uint32_t cpu_total_diff = (uint32_t)0;
	uint32_t cpu_usage_diff = (uint32_t)0;
	float cpu_usage_pct  = (float)0.0;
	uint32_t * cpu_prev_idle;
	uint32_t * cpu_prev_total;

	FILE *proc_stat_fd;
	char str_rd[HTM_STR_MAX_NUM];
	char str_cpu[HTM_CPU_ID_MAX_LEN];
	char *str_cpu_line;
	char str_err[HTM_STR_MAX_NUM];
	char *str_token;

	if (NULL == (proc_stat_fd = fopen("/proc/stat", "r")))
	{
		HTM_ERROR_PRINT_EXIT("Failed to open file /proc/stat");
	}

	if (core_id < HTM_S32V_A53_CORES_NUM)
	{
		sprintf(str_cpu, "cpu%d", core_id);
		cpu_prev_idle = &HTM_CPU_Core_Usage_Prev_Idle[core_id];
		cpu_prev_total = &HTM_CPU_Core_Usage_Prev_Totoal[core_id];
	}
	else
	{
		strcpy(str_cpu, "cpu ");
		cpu_prev_idle = &HTM_CPU_Usage_Prev_Idle;
		cpu_prev_total = &HTM_CPU_Usage_Prev_Totoal;
	}

	while (fgets(str_rd, HTM_STR_MAX_NUM, proc_stat_fd) != NULL)
	{
		str_cpu_line = strstr(str_rd, str_cpu);
		if (str_cpu_line != NULL)
		{
			/* Get the first token */
			str_token = strtok(str_cpu_line, HTM_CPU_LINE_DELIMITER);
			/* Walk through the string to get other tokens */
			while (str_token != NULL)
			{
				str_token = strtok(NULL, HTM_CPU_LINE_DELIMITER);
				if (str_token != NULL)
				{
					cpu_kw_cnt++;
					cpu_tm_val = strtoul(str_token, NULL, 10);
					cpu_total += cpu_tm_val;
					if (4 == cpu_kw_cnt)
					{
						cpu_idle = cpu_tm_val;
					}
				}
			}
			cpu_idle_diff = cpu_idle - *cpu_prev_idle;
			cpu_total_diff = cpu_total - *cpu_prev_total;
			*cpu_prev_idle = cpu_idle;
			*cpu_prev_total = cpu_total;

			cpu_usage_diff = (cpu_total_diff - cpu_idle_diff) * 100;
			cpu_usage_pct = (float)cpu_usage_diff/(float)cpu_total_diff;
			break;
		}
	}

	if (NULL == str_cpu_line)
	{
		printf(str_err, "No %s usage information in /proc/stat", str_cpu);
	}
	fflush(stdout);
	fclose(proc_stat_fd);

	return cpu_usage_pct;
}
