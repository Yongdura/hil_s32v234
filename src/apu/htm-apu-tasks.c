/*****************************************************************************
						   PVCS REVISION INFO
NOTE: Use keyword expansion here.  Replace the ‘*’ in the five lines following
 this sentence with ‘$’.  You can also remove this note from the template. 
 These are required, others can be added as desired.

*Revision*
*Author*
*Date*
*Modtime*
*Archive*

*******************************************************************************
									FILE INFO
Purpose:
    <Purpose>

Function List:
    <Function List>
                                                         
Design Document:
	<Title> 

Notes:
                                                         

*******************************************************************************
						  PROPRIETARY NOTICE

   This copyrighted document is the property of DURA Automotive and is
   disclosed in confidence.  It may not be copied, disclosed to others,
   or used for manufacturing, without the prior written consent of
   DURA Automotive.

					  Copyright 2017 DURA Automotive
******************************************************************************/

/******************************************************************************
*   
*   INCLUDE FILES
*
******************************************************************************/
#include "had-testing-mode.h"

/******************************************************************************
*   
*   PRIVATE FUNCTION DECLARATIONS
*
******************************************************************************/
static TASK_ENTRY_PTR_T Apex_Task_Entry_Table[] = {
		(TASK_ENTRY_PTR_T)&Task_Apex_Add,
		(TASK_ENTRY_PTR_T)&Task_Apex_Face_Detection_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Downsample_Upsample_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Fast9_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Gauss5x5_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Histogram_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Indirect_Input_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Integral_Image_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Irq,
		(TASK_ENTRY_PTR_T)&Task_Apex_Isp_Fast9,
		(TASK_ENTRY_PTR_T)&Task_Apex_Orb_cv,
		(TASK_ENTRY_PTR_T)&Task_Apex_Roi_cv,
};

static char * HTM_Apu_Task_Name[] = {
	"APEX ADD",
	"APEX FACE DETECTION CV",
	"APEX DOWNSAMPLE UPSAMPLE CV",
	"APEX FAST9 CV",
	"APEX GAUSS5x5 CV",
	"APEX HISTOGRAM CV",
	"APEX INDIRECT INPUT CV",
	"APEX INTEGRAL IMAGE CV",
	"APEX IRQ",
	"APEX ISP FAST9",
	"APEX ORB CV",
	"APEX ROI CV",
};


/******************************************************************************
*   
*   CODE
*
******************************************************************************/

/******************************************************************************
* FUNCTION NAME: Apex_Test
*
* PURPOSE FOR FUNCTION: 1. Process to run tasks on APU
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
int32_t Apex_Test(void)
{
	int32_t i;
	uint32_t core_id = (uint32_t)0;
	TASK_INFO_T *thread_info[HTM_S32V_APU_TASKS_NUM];
	for(i = 0; i < HTM_S32V_APU_TASKS_NUM; i++)
	{
		core_id = (uint32_t)(i/HTM_S32V_APU_TASKS_NUM_PER_CPU);
		thread_info[i] = (TASK_INFO_T *)A53_Create_Tasks(core_id, Apex_Task_Entry_Table[i]);
	/*}*/

	/* Now join with each thread, and display its returned value */
	/*for (i = 0; i < S32V_APU_TASKS_NUM; i++)
	{*/
		void *res;
		const int join_result = pthread_join(thread_info[i]->Task_ID, &res);
		if (join_result != 0)
		{
			HTM_ERROR_SAVE_PRINT_EXIT(join_result, "pthread_join");
		}
		printf("Joined with thread %d; returned value was %s\n", thread_info[i]->A53_Core_ID, (char *) res);
		/* Free memory allocated by thread */
		free(res);

		/* The buffer for threads information should be recycled,
		 * which's allocated in tasks creation */
		free(thread_info[i]);

		/* Represent the CPU core n usage */
		printf("cpu%d usage %d%\n", core_id, HTM_Get_A53_CPU_Usage(core_id));
	}

	return 0;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Add
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX ADD".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Add(void * arg)
{
	void *buffer;
	uint32_t core_id = ((TASK_INFO_T *)arg)->A53_Core_ID;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_add.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Downsample_Upsample_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX DOWNSAMPLE UPSAMPLE".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Downsample_Upsample_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_downsample_upsample_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Face_Detection_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX FACE DETECTION"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Face_Detection_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_face_detection_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Fast9_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX FAST9"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Fast9_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_fast9_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Gauss5x5_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX GAUSS5X5"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Gauss5x5_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_gauss5x5_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Histogram_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX HISTOGRAM"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Histogram_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_histogram_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Indirect_Input_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX INDIRECT INPUT"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Indirect_Input_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_indirect_input_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Integral_Image_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX INTEGRAL IMAGE"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Integral_Image_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_integral_image_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Irq
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX IRQ"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Irq(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_irq.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Isp_Fast9
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX ISP FAST9"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Isp_Fast9(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_isp_fast9.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Orb_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX ORB"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Orb_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_orb_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Apex_Roi_cv
*
* PURPOSE FOR FUNCTION: 1. Task to run application "APEX ROI"
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Apex_Roi_cv(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./apex_roi_cv.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: HTM_Apex_List_Exec_Proc
*
* PURPOSE FOR FUNCTION: 1. List processes to be executed.
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void HTM_Apex_List_Exec_Proc(void)
{
	uint32_t i;
	for(i = (uint32_t)0; i < HTM_S32V_APU_TASKS_NUM; i++)
	{
		printf("%d %s\n", i, HTM_Apu_Task_Name[i]);
	}
}