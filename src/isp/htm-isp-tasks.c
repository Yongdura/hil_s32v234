/*****************************************************************************
						   PVCS REVISION INFO
NOTE: Use keyword expansion here.  Replace the ‘*’ in the five lines following
 this sentence with ‘$’.  You can also remove this note from the template. 
 These are required, others can be added as desired.

*Revision*
*Author*
*Date*
*Modtime*
*Archive*

*******************************************************************************
									FILE INFO
Purpose:
    <Purpose>

Function List:
    <Function List>
                                                         
Design Document:
	<Title> 

Notes:
                                                         

*******************************************************************************
						  PROPRIETARY NOTICE

   This copyrighted document is the property of DURA Automotive and is
   disclosed in confidence.  It may not be copied, disclosed to others,
   or used for manufacturing, without the prior written consent of
   DURA Automotive.

					  Copyright 2017 DURA Automotive
******************************************************************************/

/******************************************************************************
*   
*   INCLUDE FILES
*
******************************************************************************/
#include "had-testing-mode.h"

/******************************************************************************
*   
*   PRIVATE FUNCTION DECLARATIONS
*
******************************************************************************/
static TASK_ENTRY_PTR_T Isp_Task_Entry_Table[] = {
		(TASK_ENTRY_PTR_T)&Task_Isp_Csi_Dcu,
		(TASK_ENTRY_PTR_T)&Task_Isp_Generic,
		(TASK_ENTRY_PTR_T)&Task_Isp_Ov10640_Quad,
		(TASK_ENTRY_PTR_T)&Task_Isp_Sony_Dualexp,
		(TASK_ENTRY_PTR_T)&Task_Isp_Viu_Dcu,
		(TASK_ENTRY_PTR_T)&Task_Isp_Rgb_Yuv_Gs8,
		(TASK_ENTRY_PTR_T)&Task_Isp_Yuv_Grey_Pyramid,
		(TASK_ENTRY_PTR_T)&Task_Isp_Jpeg,
};

static char * HTM_Isp_Task_Name[] = {
	"ISP CSI DCU",
	"ISP GENERIC",
	"ISP OV10640 QUAD",
	"ISP SONY DUALEXP",
	"ISP VIU DCU",
	"ISP RGB YUV GS8",
	"ISP YUV GREY PYRAMID",
	"ISP JPEG",
};

/******************************************************************************
* FUNCTION NAME: Isp_Test
*
* PURPOSE FOR FUNCTION: 1. Process to run tasks on ISP
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
int32_t Isp_Test(void)
{
	int32_t i;
	uint32_t core_id = (uint32_t)0;
	TASK_INFO_T *thread_info[HTM_S32V_ISP_TASKS_NUM];
	for(i = 0; i < HTM_S32V_ISP_TASKS_NUM; i++)
	{
		core_id = (uint32_t)(i/HTM_S32V_ISP_TASKS_NUM_PER_CPU);
		thread_info[i] = (TASK_INFO_T *)A53_Create_Tasks(core_id, Isp_Task_Entry_Table[i]);
	/*}*/

	/* Now join with each thread, and display its returned value */
	/*for (i = 0; i < S32V_APU_TASKS_NUM; i++)
	{*/
		void *res;
		const int join_result = pthread_join(thread_info[i]->Task_ID, &res);
		if (join_result != 0)
		{
			HTM_ERROR_SAVE_PRINT_EXIT(join_result, "pthread_join");
		}
		printf("Joined with thread %d; returned value was %s\n", thread_info[i]->A53_Core_ID, (char *) res);
		/* Free memory allocated by thread */
		free(res);

		/* The buffer for threads information should be recycled,
		 * which's allocated in tasks creation */
		free(thread_info[i]);

		/* Represent the CPU core n usage */
		printf("cpu%d usage %d%\n", core_id, HTM_Get_A53_CPU_Usage(core_id));
	}

	return 0;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Csi_Dcu
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP CSI DCU".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Csi_Dcu(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_csi_dcu.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Generic
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP GENERIC".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Generic(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_generic.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Jpeg
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP JPEG".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Jpeg(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_jpeg.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Ov10640_Quad
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP OV10640 QUAD".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Ov10640_Quad(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_ov10640_quad.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Rgb_Yuv_Gs8
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP RGB YUV GS8".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Rgb_Yuv_Gs8(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_rgb_yuv_gs8.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Sony_Dualexp
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP SONY DUALEXP".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Sony_Dualexp(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_sony_dualexp.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Viu_Dcu
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP VIU DCU".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Viu_Dcu(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_viu_dcu.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: Task_Isp_Yuv_Grey_Pyramid
*
* PURPOSE FOR FUNCTION: 1. Task to run application "ISP YUV GREY PYRAMID".
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_Isp_Yuv_Grey_Pyramid(void * arg)
{
	void *buffer;

	/* Allocate A-53 core for the task */
	buffer = A53_Alloc_Core((TASK_INFO_T *)arg);
	/* Run APEX process on APU */
	int32_t status = system("./isp_yuv_grey_pyramid.elf");

	return buffer;
}

/******************************************************************************
* FUNCTION NAME: HTM_Isp_List_Exec_Proc
*
* PURPOSE FOR FUNCTION: 1. List processes to be executed.
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void HTM_Isp_List_Exec_Proc(void)
{
	uint32_t i;
	for(i = (uint32_t)0; i < HTM_S32V_ISP_TASKS_NUM; i++)
	{
		printf("%d %s\n", i, HTM_Isp_Task_Name[i]);
	}
}
