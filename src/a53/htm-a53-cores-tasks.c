/*****************************************************************************
						   PVCS REVISION INFO
NOTE: Use keyword expansion here.  Replace the ‘*’ in the five lines following
 this sentence with ‘$’.  You can also remove this note from the template. 
 These are required, others can be added as desired.

*Revision*
*Author*
*Date*
*Modtime*
*Archive*

*******************************************************************************
									FILE INFO
Purpose:
    <Purpose>

Function List:
    <Function List>
                                                         
Design Document:
	<Title> 

Notes:
                                                         

*******************************************************************************
						  PROPRIETARY NOTICE

   This copyrighted document is the property of DURA Automotive and is
   disclosed in confidence.  It may not be copied, disclosed to others,
   or used for manufacturing, without the prior written consent of
   DURA Automotive.

					  Copyright 2017 DURA Automotive
******************************************************************************/

/******************************************************************************
*   
*   INCLUDE FILES
*
******************************************************************************/
#include "had-testing-mode.h"

/******************************************************************************
* FUNCTION NAME: A53_Test
*
* PURPOSE FOR FUNCTION: 1. Process to run tasks on A53.
*
* NAME     Direction        Description
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
int32_t A53_Test(void)
{
	int32_t i;
	TASK_INFO_T *thread_info[HTM_S32V_A53_CORES_NUM];
	for(i = 0; i < HTM_S32V_A53_CORES_NUM; i++)
	{
		thread_info[i] = (TASK_INFO_T *)A53_Create_Tasks(i, (TASK_ENTRY_PTR_T)&Task_A53_Multicore);
	}

	/* Now join with each thread, and display its returned value */
	for (i = 0; i < HTM_S32V_A53_CORES_NUM; i++)
	{
		void *res;
		const int join_result = pthread_join(thread_info[i]->Task_ID, &res);
		if (join_result != 0)
		{
			HTM_ERROR_SAVE_PRINT_EXIT(join_result, "pthread_join");
		}

		printf("Joined with thread %d; returned value was %s\n", thread_info[i]->A53_Core_ID, (char *) res);
		/* Free memory allocated by thread */
		free(res);

		/* The buffer for threads information should be recycled,
		 * which's allocated in tasks creation */
		free(thread_info[i]);

		/* Represent the CPU core n usage */
		printf("cpu%d usage %d%\n", thread_info[i]->A53_Core_ID, HTM_Get_A53_CPU_Usage(thread_info[i]->A53_Core_ID));
	}

	return 0;
}

/******************************************************************************
* FUNCTION NAME: Task_A53_Multicore
*
* PURPOSE FOR FUNCTION: 1. A task to run on a core.
*
* NAME     Direction        Description
* arg      input            Thread information including core id and thread id.
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * Task_A53_Multicore(void *arg)
{
	TASK_INFO_T *thread_info = arg;
	char *buffer;
	uint32_t cnt = (uint32_t)100000000;

	buffer = A53_Alloc_Core(thread_info);

	while(cnt > (uint32_t)0)
	{
		cnt--;
	}
	usleep(100);

	return buffer;
}


/******************************************************************************
* FUNCTION NAME: A53_Create_Tasks
*
* PURPOSE FOR FUNCTION: 1. To create a POSIX thread.
*
* NAME     Direction        Description
* core_id  input            The core to run a specified thread
* t_entry  input            The thread/task entry point
* 
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
uint32_t A53_Create_Tasks(uint32_t core_id, TASK_ENTRY_PTR_T t_entry)
{
	/* Initialize thread creation attributes */
	pthread_attr_t attr;
	const int attr_init_result = pthread_attr_init(&attr);
	if (attr_init_result != 0)
	{
		HTM_ERROR_SAVE_PRINT_EXIT(attr_init_result, "pthread_attr_init");
	}

	/* We will set the stack size limit to is 1 MB (0x100000 bytes)*/
	/*const int stack_size = 0x100000;
	const int setstacksize_result = pthread_attr_setstacksize(&attr, stack_size);
	if (setstacksize_result != 0)
	{
		HTM_ERROR_SAVE_PRINT_EXIT(setstacksize_result, "pthread_attr_setstacksize");
	}*/

	/* Allocate memory for pthread_create() arguments */
	TASK_INFO_T *thread_info = calloc((size_t)1, sizeof(TASK_INFO_T));
	if (thread_info == NULL)
	{
		HTM_ERROR_PRINT_EXIT("calloc");
	}

	/* Create the threads and initialize the core_id argument, which will be used to set the thread
	 * to the specific CPU core. */
	/* For example, we want the first pthread to camp on the first CPU core which has the ID 0. So we
	 * pass the value 0 to its core_id.*/
	{
		thread_info->A53_Core_ID = core_id;
		/* The pthread_create() call stores the thread ID into corresponding element of thread_info[] */
		const int create_result = pthread_create(&(thread_info->Task_ID),
												 &attr,
												 t_entry,
												 thread_info);
		if (create_result != 0)
		{
			HTM_ERROR_SAVE_PRINT_EXIT(create_result, "pthread_create");
		}
	}

	/* Destroy the thread attributes object, since it is no longer needed */
	const int destroy_result = pthread_attr_destroy(&attr);
	if (destroy_result != 0)
	{
		HTM_ERROR_SAVE_PRINT_EXIT(destroy_result, "pthread_attr_destroy");
	}

	return (uint32_t)((TASK_INFO_T *)thread_info);
}

/******************************************************************************
* FUNCTION NAME: A53_Alloc_Core
*
* PURPOSE FOR FUNCTION: 1. Assign a core to a thread and move the thread to the 
                           core.
*
* NAME     Direction        Description
* t_info   input            Thread information including core id and thread id.
*
*  ----------------------
*  - HISTORY OF CHANGES -
*  ----------------------
*    Date       Software Version    Initials    Description
*
*
******************************************************************************/
void * A53_Alloc_Core(TASK_INFO_T *t_info)
{
	if (NULL == t_info)
	{
		printf("No thread information available~~~\n");
		return NULL;
	}

	/* Move the task to a specified CPU core */
	TASK_INFO_T *thread_info = t_info;

	const pthread_t pid = pthread_self();
	const int core_id = thread_info->A53_Core_ID;

	/* cpu_set_t: This data set is a bitset where each bit represents a CPU. */
	cpu_set_t cpuset;
	/* CPU_ZERO: This macro initializes the CPU set set to be the empty set. */
	CPU_ZERO(&cpuset);
	/* CPU_SET: This macro adds cpu to the CPU set set. */
	CPU_SET(core_id, &cpuset);

	/* pthread_setaffinity_np:
	 * The pthread_setaffinity_np() function sets the CPU affinity mask of the thread to
	 * the CPU set pointed to by cpuset. If the call is successful, and the thread is not currently
	 * running on one of the CPUs in cpuset, then it is migrated to one of those CPUs. */
	const int set_result = pthread_setaffinity_np(pid, sizeof(cpu_set_t), &cpuset);
	if (set_result != 0)
	{
		HTM_ERROR_SAVE_PRINT_EXIT(set_result, "pthread_setaffinity_np");
	}

	/* Check what is the actual affinity mask that was assigned to the thread. */
	/* pthread_getaffinity_np:
	 * The pthread_getaffinity_np() function returns the CPU affinity mask of the thread thread
	 * in the buffer pointed to by cpuset. */
	const int get_affinity = pthread_getaffinity_np(pid, sizeof(cpu_set_t), &cpuset);
	if (get_affinity != 0)
	{
		HTM_ERROR_SAVE_PRINT_EXIT(get_affinity, "pthread_getaffinity_np");
	}

	char *buffer;
	/* CPU_ISSET: This macro returns a nonzero value (true) if cpu is a member of the CPU set,
	 * and zero (false) otherwise. */
	if (CPU_ISSET(core_id, &cpuset))
	{
		const size_t needed = snprintf(NULL, 0, HTM_AFFIN_SUCCESS_MSG, pid, core_id);
		buffer = malloc(needed);
		snprintf(buffer, needed, HTM_AFFIN_SUCCESS_MSG, pid, core_id);
	}
	else
	{
		const size_t needed = snprintf(NULL, 0, HTM_AFFIN_FAILURE_MSG, pid, core_id);
		buffer = malloc(needed);
		snprintf(buffer, needed, HTM_AFFIN_FAILURE_MSG, pid, core_id);
	}

	return (void *)buffer;
}
