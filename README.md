# README #

This README would normally document whatever steps are necessary to get your application up and running.

This application is used to help hardware team to verify hardware thermal requirement.

The application creates tasks running on assigned A-53 cores.

1. A53 four cores
Create four threads and then move them to four cores. In each thread, a while loop to simulate intesive computation to measure CPU usage.

2. Apex Processors
Create four threads and then move them to four A-53 cores. In each thread, a APEX process would be executedd. There is no way to measure APEX cores usage, but we just continously send tasks to APEX cores and keep it busy.

3. ISP Processors
Create four threads and then move them to four A-53 cores. In each thread, a ISP process would be executedd. There is no way to measure ISP cores usage, but we just continously send tasks to ISP cores and keep it busy.

4. GPU Processors (No implementation)

